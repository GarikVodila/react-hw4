import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    cart: [],
};

export const cartSlice = createSlice({
    name: 'cart',
    initialState,
    reducers: {
        actionSetCart(state, action) {
            state.cart = [...action.payload];
        },
    },
});

export const { actionSetCart } = cartSlice.actions;

export default cartSlice.reducer;
