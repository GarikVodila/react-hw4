import { createSlice } from '@reduxjs/toolkit';
import { element, string } from 'prop-types';

const initialState = {
    favorites: [],
};

export const favoritesSlice = createSlice({
    name: 'favorites',
    initialState,
    reducers: {
        actionSetFavorites(state, action) {
            //  state.favorites = action.payload;
            // if (!state.favorites.includes(action.payload)) {
            //     setFavorites([...favorites, item]);
            // } else { 
            //      const updateFavorites = favorites.filter((el) => el !== item);
            //      setFavorites([...updateFavorites]);
            //  }
            // console.log(action.payload);
            // if (state.favorites.length === 0) {
            //     state.favorites = [action.payload]
            // } else {
            //     console.log("else")
                // const newState = state.favorites;
                // const newAction = action.payload;
                // const existingItem = newState.find(items => items.article === newAction.article);
                // console.log(existingItem[0]);
                // if (existingItem) {
                //     state.favorites = [...newState, existingItem];
                // }
                // const updateFavorites = state.favorites.forEach((el, index, arr) => 
                // {
                    // el.article !== action.payload.article;
                    // if (el.article === action.payload.article) {
                    //     el.remove();
                    // } else {
                    //     arr.push(action.payload);
                    // }
                // });
                // const updateFavorites = state.favorites.filter((el) => el.article !== action.payload.article);
                // state.favorites = [...updateFavorites];
                // console.log(state.favorites)
                // typeof action.payload
                console.log('action payload', action.payload);
                
                const findEl = state.favorites.find(el => el.article === action.payload.article);
                if (!findEl || findEl.length === 0 ) {
                    localStorage.setItem("favorites", JSON.stringify([...state.favorites, action.payload]));
                    state.favorites = [...state.favorites, action.payload];
                } else {
                    const filterFevorites = state.favorites.filter(el => el.article !== action.payload.article);
                    localStorage.setItem("favorites", JSON.stringify(filterFevorites));
                    state.favorites = [...filterFevorites]
                }
            }
        },
    },
);

export const { actionSetFavorites } = favoritesSlice.actions;

export default favoritesSlice.reducer;
