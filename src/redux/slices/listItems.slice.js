import { createSlice } from '@reduxjs/toolkit';
import { sendRequest } from '../../helpers/sendRequest';

const initialState = {
    listItems: [],
};

export const listItemsSlice = createSlice({
    name: 'listItems',
    initialState,
    reducers: {
        actionSetListItems(state, action) {
            state.listItems = action.payload;
        },
    },
});

export const { actionSetListItems } = listItemsSlice.actions;

export const actionFetchListItems = async (dispatch) => {
    const data = await sendRequest('/data.json');
    return dispatch(actionSetListItems(data));
};

export default listItemsSlice.reducer;
