import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

import { ReactComponent as Logo } from './icons/logo.svg';
import { ReactComponent as Favorites } from './icons/favorite.svg';
import { ReactComponent as Cart } from './icons/cart.svg';

import './Header.scss';

function Header({ qtyFavorites, qtyCart }) {
    return (
        <div className="header__wrapper">
            <div className="container">
                <div className="header">
                    <NavLink to="/" className="header__logo">
                        <h1 className="header__logo--title">SHOES</h1>
                        <Logo className="header__logo--icon" />
                    </NavLink>
                    <div className="header__actions">
                        <NavLink to="/favorites" className="actions__favorite--wrapp" >
                            <Favorites className="actions__favorite" />
                            <span className="actions__favorite--qty">{qtyFavorites}</span>
                        </NavLink>
                        <NavLink to="/cart" className="actions__cart--wrapp" >
                            <Cart className="actions__cart" />
                            <span className="actions__cart--qty">{qtyCart}</span>
                        </NavLink>
                    </div>
                </div>
            </div>
        </div>
    );
}

Header.propTypes = {
    qtyFavorites: PropTypes.any,
    qtyCart: PropTypes.any,
};

export default Header;
