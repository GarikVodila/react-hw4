import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import { useDispatch, useSelector} from 'react-redux';
import { actionSetFavorites } from '../../../../redux/slices/favorites.slice'

import { ReactComponent as Favorite } from '../../../Header/icons/favorite.svg';
import { Button } from '../../../Button';
import { Modal } from '../../../Modal';

import './ItemBlock.scss';

function ItemBlock(props) {
    const { currentItem, title, price, url, article, color, cart, addToFavorites, addToCart } = props;



    const dispatch = useDispatch();
    const favorites = useSelector(state => state.favorites.favorites);





    const [isShowModal, isSetShowModal] = React.useState(false);

    const handleModal = () => {
        if (!isShowModal) {
            document.querySelector('body').classList.add('body-no-scroll');
            isSetShowModal(isShowModal => !isShowModal);
        } else {
            document.querySelector('body').classList.remove('body-no-scroll');
            isSetShowModal(isShowModal => !isShowModal);
        }
    };

    const handleOutside = event => {
        if (!event.target.closest('.modal__content')) {
            handleModal();
        }
    };

    return (
        <>
            <div className="item__block">
                <span className="item__article">art. {article}</span>
                <h1 className="item__title">{title}</h1>
                <img className="item__img" src={url} alt="item-img" />
                <div className="item__bottom">
                    <span className="item__color">{color}</span>
                    <Favorite
                        className={cn('item__favorite', {
                            // active: favorites.includes(article),
                            // active: favorites.includes(currentItem),
                        })}
                        onClick={() => {
                            // addToFavorites(currentItem)
                            dispatch(actionSetFavorites(currentItem))
                            console.log(currentItem)
                            console.log('Favorites from ItemBlock', favorites);
                        }}
                    />
                </div>
                <div className="item__price">{price} ₴</div>
                {!cart.includes(article) && (
                    <Button className="item__btn" onClick={() => handleModal()}>
                        Add to cart
                    </Button>
                )}
                {cart.includes(article) && (
                    <Button className="item__btn" disabled>
                        Added
                    </Button>
                )}
            </div>
            {isShowModal && (
                <Modal
                    title="Add to cart"
                    handleModal={handleModal}
                    handleOutside={handleOutside}
                    addToCart={addToCart}
                    articleForCart={article}
                >
                    <div>Are you sure?</div>
                </Modal>
            )}
        </>
    );
}

ItemBlock.propTypes = {
    item: PropTypes.object,
    title: PropTypes.string,
    price: PropTypes.number,
    url: PropTypes.string,
    article: PropTypes.number,
    color: PropTypes.string,
    // favorites: PropTypes.array,
    cart: PropTypes.array,
    addToFavorites: PropTypes.func,
    addToCart: PropTypes.func,
};

export default ItemBlock;
